# elaboration

A repo is used to elaborate on articles/tutorials in greater detail

### Motivation 
I find that the key to learning is to internalize abstractions. Abstractions can
only be internalized by gaining a deep understanding of what is concrete
(e.g the examples). Because of the curse of knowledge, sometimes the examples
presented to us in articles leave too large a knowledge gap between author and 
reader to be able to grasp the examples adaquately. This often leads to 
confusion, frustration, and calling it quits.

I aim to use this repo to jot down the intermediary steps, to help bridge that
gap. I'm doing the mental gymnastics to figure this stuff out anyways, so might
as well write it down so that others might benefit.


### Thank you to

* Everyone who has taken the time to write these awesome articles
* http://heckyesmarkdown.com/ for the html to markdown conversion
